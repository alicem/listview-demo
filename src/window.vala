public class ListviewDemo.MyObject : Object {
    public string title { get; set; }
    public string description { get; set; }
}

public class ListviewDemo.LazyModel : Object, ListModel {
    private Type get_item_type () {
        return typeof (MyObject);
    }

    private uint get_n_items () {
        return 10000000;
    }

    private Object? get_item (uint index) {
        return new MyObject () {
            title = @"Title $index",
            description = @"Description $index"
        };
    }
}

[GtkTemplate (ui = "/org/example/App/window.ui")]
public class ListviewDemo.Window : Adw.ApplicationWindow {
    public Window (Gtk.Application app) {
        Object (application: app);
    }

    static construct {
        typeof (LazyModel).ensure ();
    }
}
