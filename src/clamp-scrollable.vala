public class ListviewDemo.ClampScrollable : Gtk.Widget, Gtk.Scrollable, Gtk.Buildable {
    private Gtk.Widget? _child;
    public Gtk.Widget? child {
        get { return _child; }
        set {
            if (child == value)
                return;

            if (child != null) {
                hadjustment_binding.unbind ();
                vadjustment_binding.unbind ();
                hscroll_policy_binding.unbind ();
                vscroll_policy_binding.unbind ();

                hadjustment_binding = null;
                vadjustment_binding = null;
                hscroll_policy_binding = null;
                vscroll_policy_binding = null;

                child.unparent ();
            }

            _child = value;

            if (child != null) {
                child.set_parent (this);

                hadjustment_binding = child.bind_property ("hadjustment", this, "hadjustment", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
                vadjustment_binding = child.bind_property ("vadjustment", this, "vadjustment", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
                hscroll_policy_binding = child.bind_property ("hscroll_policy", this, "hscroll_policy", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
                vscroll_policy_binding = child.bind_property ("vscroll_policy", this, "vscroll_policy", BindingFlags.SYNC_CREATE | BindingFlags.BIDIRECTIONAL);
            }
        }
    }

    public Gtk.Adjustment hadjustment { get; construct set; }
    public Gtk.Adjustment vadjustment { get; construct set; }
    public Gtk.ScrollablePolicy hscroll_policy { get; set; }
    public Gtk.ScrollablePolicy vscroll_policy { get; set; }

    private Binding hadjustment_binding;
    private Binding vadjustment_binding;
    private Binding hscroll_policy_binding;
    private Binding vscroll_policy_binding;

    static construct {
        set_layout_manager_type (typeof (Hdy.ClampLayout));
    }

    protected override void dispose () {
        child = null;

        base.dispose ();
    }

    private void add_child (Gtk.Builder builder, Object object, string? type) {
        if (object is Gtk.Scrollable && object is Gtk.Widget)
            child = object as Gtk.Widget;
        else
            base.add_child (builder, object, type);
    }

    private bool get_border (out Gtk.Border border) {
        border = {};

        return false;
    }
}
